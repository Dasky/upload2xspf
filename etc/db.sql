-- phpMyAdmin SQL Dump
-- version 2.10.1
-- http://www.phpmyadmin.net
-- 
-- Serveur: localhost
-- Généré le : Mer 22 Janvier 2014 à 00:28
-- Version du serveur: 5.1.32
-- Version de PHP: 5.3.3


CREATE DATABASE medias;

use medias;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de données: `medias`
-- 

-- --------------------------------------------------------

--
-- Structure de la table `tconfig`
--

DROP TABLE IF EXISTS `tconfig`;
CREATE TABLE `tconfig` (
  `conf_i_id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_e_type` enum('video','audio') NOT NULL DEFAULT 'video',
  `conf_s_domain` varchar(400) NOT NULL,
  PRIMARY KEY (`conf_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table 'tconfig'
--

-- --------------------------------------------------------

-- 
-- Structure de la table `tmedia`
-- 

DROP TABLE IF EXISTS `tmedia`;
CREATE TABLE `tmedia` (
  `med_i_id` int(11) NOT NULL AUTO_INCREMENT,
  `med_s_title` varchar(400) NOT NULL,
  `med_s_artist` text NOT NULL,
  `med_s_description` text,
  `med_e_type` enum('video','audio') DEFAULT NULL,
  `med_s_url` varchar(500) NOT NULL,
  `med_d_created` datetime DEFAULT NULL,
  `med_d_updated` datetime DEFAULT NULL,
  `pro_i_id` int(11) NOT NULL,
  PRIMARY KEY (`med_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Contenu de la table `tmedia`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `tplaylist`
-- 

DROP TABLE IF EXISTS `tplaylist`;
CREATE TABLE `tplaylist` (
  `pla_i_id` int(11) NOT NULL AUTO_INCREMENT,
  `pla_d_created` datetime NOT NULL,
  `rec_s_oai` varchar(400) NOT NULL,
  `med_i_id` int(11) NOT NULL,
  `pla_i_order` int(11) NOT NULL,
  `pro_i_id` int(11) NOT NULL,
  PRIMARY KEY (`pla_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Contenu de la table `tplaylist`
-- 


-- --------------------------------------------------------

-- 
-- Structure de la table `tprovider`
-- 

DROP TABLE IF EXISTS `tprovider`;
CREATE TABLE `tprovider` (
  `pro_i_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_s_code` varchar(50) NOT NULL,
  `pro_s_name` varchar(50) NOT NULL,
  `pro_s_email` varchar(45) NOT NULL,
  `pro_s_password` varchar(64) NOT NULL,
  `pro_i_status` tinyint(4) NOT NULL DEFAULT '0',
  `pro_d_created` datetime NOT NULL,
  PRIMARY KEY (`pro_i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Contenu de la table `tprovider`
-- 


