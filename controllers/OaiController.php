<?php
/**
 * OaiController
 * @author Dorian Yahouédéou <dorian@poc2prod.com>
 */
class OaiController extends \Temma\Controller {

	private $_playlistBaseUrl = null;

	public function init() {
		// get the base playlistUrl
		$this->_playlistBaseUrl = $this->_config->xtra('playlist', 'baseUrl');
	}

	public function execGetRecord() {
		if (empty($_GET) || !isset($_GET['url'])) {
			$this->httpError(404);
			return (self::EXEC_HALT);
		}
		$record = file_get_contents($_GET['url']);
		$xml = simplexml_load_string($record);
		$xml->registerXPathNamespace("n", "http://www.openarchives.org/OAI/2.0/");
		$related=$xml->xpath('//n:OAI-PMH/n:GetRecord/n:record/n:metadata/n:mods/n:relatedItem[@type="constituent"]');
		$titleParts=array();
		$nameParts=array();
		$notFilles=array();
		foreach ($related as $part=>$rel){
			if (isset($rel->identifier)) {
				foreach($rel->identifier->attributes() as $att => $val) { 
					if ($att == "displayLabel") {	
						$notFilles[$part]['label'] = "" . $val;
					}	
				}
				$notFilles[$part]['uri']= "" . $rel->identifier;
			} else {
				$titleInfo = $rel->titleInfo;
				if (isset($titleInfo)) {
					$titleParts[$part] .= isset($rel->titleInfo->title) ? $rel->titleInfo->title : "";
					$titleParts[$part] .= isset($rel->titleInfo->subTitle) ? $rel->titleInfo->subTitle : "";
				} else 
					$titleParts[$part] = "";
				$nameCount=0;
				foreach ($rel->name as $name) {
					if ($nameCount!=0) 
						$nameParts[$part] .= " ; ";
					$nameCount++;		
					$nameParts[$part] .= isset($name->namePart) ? $name->namePart : $name->displayForm;
					$nameParts[$part] .= isset($name->role->roleTerm) ? " (".$name->role->roleTerm.")" : "";
				}
				if (!isset($nameParts[$part])) 
					$nameParts[$part]= "";
			}
		}
		$res = array();
		preg_match('/identifier=(.*)/', $_GET['url'], $matches);
		$res['oaiIdentifier'] = $matches[1];	
		// If it exists one or more titleParts
		if (!empty($titleParts)) { 
			foreach ($titleParts as $pos => $title) {
				$res['items'][] = array(
					'title' => $title,
					'artist'  => $nameParts[$pos]
				);
			}
		} else {
		// If not, use the record title and the first available artist
			$title = 'Titre inconnu';
			$name = 'Artist inconnu';
			$recordDataTitle = $xml->xpath('//n:OAI-PMH/n:GetRecord/n:record/n:metadata/n:mods/n:titleInfo/n:title');
			if (!empty($recordDataTitle))
				foreach ($recordDataTitle as $title) {
					$title = (string) $title;
				}
			$recordDataNames = $xml->xpath('//n:OAI-PMH/n:GetRecord/n:record/n:metadata/n:mods/n:name/n:namePart');
			if (!empty($recordDataNames))
				foreach ($recordDataNames as $nameItem) {
					$names[] = (string) $nameItem;
				}
			$name = empty($names) ? $name : $names[0];
			$res['items'][] = array(
				'title' 	=> $title,
				'artist' 	=> $name
			);	
		}
		$res['playlistBaseUrl'] = $this->_playlistBaseUrl;
		$this->view('Temma\Views\JsonView');
		$this->set('json', $res);
	}
}

?>
