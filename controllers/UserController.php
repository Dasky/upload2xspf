<?php
/**
 * UserController
 * TODO > finalize the Token generation.
 * @author Dorian Yahouédéou <dorian@poc2prod.com>
 */
class UserController extends \Temma\Controller {

        /** User Dao */
        private $_userDao = null;

	public function init() {
		$this->_userDao = $this->loadDao('User\UserDao');
	}	
	/**
	 * Authenticate a user.
	 */
	public function execAuthenticate() {
		$this->view('\Temma\Views\JsonView');
		$params = json_decode(file_get_contents('php://input'));
		$params = (array)$params;
		$login = $params['login'];
		$password = $params['password'];
		// check the authentication
		$userData = $this->_userDao->logIn($login, $password);
		// if auth failure
		if (is_null($userData)) {
			$this->httpError('401');
			return;
		}	
		// if auth success
		$authData = array(
			'id' 		=> $userData['id'],
			'code'		=> $userData['code'],
			'connected'	=> 1,
			'token'		=> 'my_beautiful_token'
		);
		$this->set('json', $authData);
	}
	public function execDisconnect($userId) {
		$this->view('\Temma\Views\JsonView');
		$this->_userDao->logOut($userId);
		$this->set('json', true);
		return;
	}
	public function execGetStatus() {
		$this->view('Temma\Views\JsonView');
		$headers = getallheaders();
		$authorization = $headers['Authorization'];
		$requestId = $headers['id'];
		if ((!isset($requestId) || is_null($requestId)) && !is_int($requestId)) {
			$this->httpError('401');
			return;
		}	
		$userData = $this->_userDao->getStatus($requestId);
//TODO
/* Check if the token from userData is correct
$user_token = "my_beautiful_token";
		//if (!$status || $token != $user_token) {
		if ($authorization != $user_token) {
                        $this->httpError('404');
                        return;
		}			
*/
                // if auth success
                $authData = array(
                        'id'            => $userData['id'],
                        'code'          => $userData['code'],
                        'connected'     => 1,
                        'token'         => 'my_beautiful_token'
                );
		$this->set('json', $authData);
		return;
	}
}

?>
