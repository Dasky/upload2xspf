<?php
/**
 * PlaylistController
 * @author Dorian Yahouédéou <dorian@poc2prod.com>
 */
class PlaylistController extends \Temma\Controller {

	private $_playlistDao = null;
	private $_baseUrl = null;
	
	public function init() {
		$this->_playlistDao = $this->loadDao('Playlist\PlaylistDao');	
		$this->_baseUrl = $this->_config->xtra('streaming', 'baseUrl');
	}

	public function execGet() {
		$this->view('Temma\Views\JsonView');
		$oaiIdentifier = $_GET['oaiIdentifier'];
		$providerId = $_GET['providerId'];
		if (empty($oaiIdentifier) || is_null($oaiIdentifier) || empty($providerId) || is_null($providerId)) {
			$this->httpError('404');
			return;
		}
		$playlist = $this->_playlistDao->get($oaiIdentifier);
		$this->set('json', $playlist);
		return;
	}
	
	public function execRemoveTrack() {
		$this->view('Temma\Views\JsonView');
		$trackId = $_GET['trackId'];
		$this->_playlistDao->removeTrack($trackId);
		$this->set('json', true);
		return;
	}
	public function execRecord($oaiIdentifier) {
		if (empty($oaiIdentifier) || is_null($oaiIdentifier)) {
			$this->httpError('404');
			return;
		}
		$playlist = $this->_playlistDao->get($oaiIdentifier);
		$xmlHeader = '<?xml version="1.0" encoding="UTF-8"?>';
		$xmlHeader .= '<playlist xmlns="http://xspf.org/ns/0/" xmlns:jwplayer="http://developer.longtailvideo.com" version="1"><trackList>';
		$xmlFooter = '</trackList></playlist>';
		$xmlItems = '';
		foreach ($playlist as $track) {
			$xmlItems .= '<track><title>'. $track['title']  
					. '</title><annotation>' . $track['artist'] .'</annotation>'
					. '<creator>' . $track['artist'] .'</creator>'
					. '<location>' . $this->_baseUrl . $track['url'] . '</location>'
					. '</track>';
		}
		$xmlStream = $xmlHeader.$xmlItems.$xmlFooter;
		//TODO definitive header : header('Content-type: application/xspf+xml');
		header('Content-type: application/xml');
		echo $xmlStream;
	}
}
?>
