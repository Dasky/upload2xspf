<?php

class HomepageController extends \Temma\Controller {

	public function init() {}

	public function execIndex() {
		$this->view('Temma\Views\PhpView');
		//$productDAO = $this->loadDao('Product\ProductDAO');
		$this->template('homepage/index.html');
	}
	public function execGettracks() {
		$this->view('Temma\Views\JsonView');
		$jsonResponse = array(
					array("title"	=> "This is love",
					      "creator"	=> "Bob marley",
					      "size"	=> "2"
					),
					array("title"	=> "Imagine",
					      "creator"	=> "John lennon",
					      "size"	=> "3"
					),
					array("title"	=> "Everyday",
					      "creator" => "Camille",
					      "size"	=> "4"
					)
		);
		$this->set('json', $jsonResponse);
	}	
}

?>
