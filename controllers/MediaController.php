<?php
/**
 * MediaController
 * @author Dorian Yahouédéou <dorian@poc2prod.com>
 */
class MediaController extends \Temma\Controller {

	private $_mediaDao = null;

	public function init() {
	}

	public function execPublish() {
		$this->_mediaDao = $this->loadDao('Media\MediaDao');	
		$this->view('Temma\Views\JsonView');
		$params = json_decode(file_get_contents('php://input'));
		$params = (array) $params;
		if (empty($params))
			$this->set('json', false);
		preg_match('/identifier=(.*)/', $params['oaiUrl'], $matches);
		if (empty($matches) || !isset($matches[1]))
			$this->set('json', true);
		$oaiIdentifier = $matches[1];
//TODO : 1. extract the media title and artist
		$providerId = $params['providerId'];
		$providerCode = $params['providerCode'];
		$mediaType = 'video'; // enforce to "video" while there is no other media types
		$relatedItems = (array)$params['relatedItems'];
		$relatedItems = (array) $relatedItems['object'];
		$title = $relatedItems['title'];
		$artist = !empty($relatedItems['artist']) ? $relatedItems['artist'] : 'n/a';
		$filepath = $providerCode . '/'. $mediaType . '/' . $params['filename'];
//TODO : 2. add the data to database
		// if no authData provided
		if (is_null($providerId) || is_null($providerCode)) {
			$this->set('json', false);
			return;
		}
		$this->_mediaDao->add($providerId, $providerCode, $oaiIdentifier, $title, $artist, $filepath);
		$this->set('json', true);
	}
}

?>
