<?php

namespace Media;

class MediaDao extends \Temma\Dao {
	
	public function add($providerId, $providerCode, $oaiIdentifier, $title, $artist, $filepath) {
                // current date time
                $currentDate = date("Y-m-d H:i:s");
                // check if there is already a related playlist
                $sql = "SELECT pla_i_id as playlistId
                        FROM medias.tplaylist
                        WHERE rec_s_oai = '" . $this->_db->quote($oaiIdentifier) . "'";
                $checkRes = $this->_db->queryAll($sql);
                // insert the media reference
                $sql = "INSERT INTO medias.tmedia
                        SET med_s_title = '" . $this->_db->quote($title)  . "',
                        med_s_artist = '" . $this->_db->quote($artist) . "',
                        med_s_url = '" . $this->_db->quote($filepath) . "',
                        pro_i_id = " . $providerId . ",
                        med_d_created = '" . $this->_db->quote($currentDate) . "',
                        med_d_updated = '" . $this->_db->quote($currentDate) . "'";
                $this->_db->exec($sql);
                // add the media to the playlist
                $mediaId = $this->_db->lastInsertId();
                $nbMedias = count($checkRes);
                if ($nbMedias == 0)
                        $order = 1;
                else
                        $order = $nbMedias + 1;
                $sql = "INSERT INTO medias.tplaylist
                        SET rec_s_oai = '" . $this->_db->quote($oaiIdentifier) . "',
                        med_i_id = " . $this->_db->quote($mediaId) . ",
                        pla_i_order = " . $this->_db->quote($order)  . ",
                        pro_i_id = '" . $providerId . "',
                        pla_d_created = '" . $this->_db->quote($currentDate) . "'";
                $this->_db->exec($sql);	
	}
}

?>
