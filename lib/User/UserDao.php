<?php

namespace User;

class UserDao extends \Temma\Dao {

	/** Check and get user credentials */
	public function logIn($login, $password) {
                $sql = "SELECT  pro_i_id as id,
                                pro_s_code as code
                        FROM medias.tprovider   
                        WHERE pro_s_code='" . $this->_db->quote($login) . "'
                          AND pro_s_password='" . $this->_db->quote($password) . "'";
                $authRes = $this->_db->queryOne($sql);
		if (is_null($authRes))
			return (null);
		// log the user in
		$sql = "UPDATE medias.tprovider
			SET pro_i_status = 1
			WHERE pro_i_id = '" . $this->_db->quote($authRes['id'])  . "'";
		$this->_db->exec($sql);
		return ($authRes);
	}
	public function logOut($id) {
		// log the user in
		$sql = "UPDATE medias.tprovider
			SET pro_i_status = 0
			WHERE pro_i_id = '" . $id . "'";
		$this->_db->exec($sql);	
	}
	public function getStatus($id) {
		$sql = "SELECT 	pro_i_id as id,
				pro_s_code as code
			FROM medias.tprovider
			WHERE pro_i_id = '" . $this->_db->quote($id)  . "'
			 AND pro_i_status = 1";
		$user = $this->_db->queryOne($sql);
		if (empty($user))
			return (false);
		return ($user);
	}
}

?>
