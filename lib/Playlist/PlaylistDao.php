<?php

namespace Playlist;

class PlaylistDao extends \Temma\Dao {
	
	public function get($oaiIdentifier) {
   		// get the playlist data
		$sql = "SELECT 	pla_i_id as trackId,
				pla_d_created as dateCreated,
				med_i_id as mediaId,
				med_s_title as title,
				med_s_artist as artist,
				med_s_url as url,
				rec_s_oai as oaiIdentifier,
				pla_i_order as position
			FROM medias.tplaylist
			INNER JOIN medias.tmedia USING(med_i_id)
			WHERE rec_s_oai = '" . $this->_db->quote($oaiIdentifier)  . "'
			ORDER BY pla_i_order ASC";
		$playlist = $this->_db->queryAll($sql);
		return ($playlist);	
	}
	
	public function removeTrack($trackId) {
		$sql = "DELETE FROM medias.tplaylist
			WHERE pla_i_id = '" . $this->_db->quote($trackId) . "'";
		$this->_db->exec($sql);
	}
}

?>
