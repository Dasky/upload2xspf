<?php

error_reporting(0); // Set E_ALL for debuging

include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderConnector.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinder.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeDriver.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeLocalFileSystem.class.php';
// Required for MySQL storage connector
// include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeMySQL.class.php';
// Required for FTP connector support
// include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeFTP.class.php';


/**
 * Simple function to demonstrate how to control file access using "accessControl" callback.
 * This method will disable accessing files/folders starting from  '.' (dot)
 *
 * @param  string  $attr  attribute name (read|write|locked|hidden)
 * @param  string  $path  file path relative to volume root directory started with directory separator
 * @return bool|null
 **/
function access($attr, $path, $data, $volume) {
	return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
		? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
		:  null;                                    // else elFinder decide it itself
}

/**
 * Addon to define the path and URL to files
 * @author	Dorian Yahouédéou
 */
// Get the parameters
$redirectUrl = $_SERVER['REDIRECT_URL'];
preg_match('/\/connector\/(.*)/', $redirectUrl, $matches);
$providerCode = null;
if (!empty($matches) && !is_null($matches))
	$providerCode = $matches[1];
// Define the paths
$mediaType = 'video'; //enforced to video while this is the only availble media
$sourcePath = '../www/files/' . $providerCode . '/' . $mediaType;
$destinationPath = '/var/www/gamop/www/files/streaming/' . $providerCode . '/' . $mediaType;
$opts = array(
	// 'debug' => true,
	'roots' => array(
		array(
			'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
			'path'          => $sourcePath,        // path to files (REQUIRED)
			'URL'           => dirname($_SERVER['PHP_SELF']) . $sourcePath, // URL to files (REQUIRED)
			'alias'		=> 'Médias disponibles',
			'accessControl' => 'access',             // disable and hide dot starting files (OPTIONAL)
		),
		array(
			'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
			'path'          => $destinationPath, // path to files (REQUIRED)
			'URL'           => dirname($_SERVER['PHP_SELF']) . $destinationPath, // URL to files (REQUIRED)
			'alias'		=> 'Médias en ligne',
			'accessControl' => 'access',             // disable and hide dot starting files (OPTIONAL)
			'defaults'	=> array('read' => false), //TODO
		)
		
	)
);

// run elFinder
$connector = new elFinderConnector(new elFinder($opts));
$connector->run();

