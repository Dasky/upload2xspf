<?php

namespace Temma\Exceptions;

/**
 * Objet de gestion des exceptions de Temma lors d'une erreur HTTP.
 *
 * @author	Amaury Bouchard <amaury.bouchard@finemedia.fr>
 * @copyright	© 2010-2011, FineMedia
 * @package	Temma
 * @subpackage	Exceptions
 * @version	$Id$
 */
class HttpException extends \Exception {
}

?>
