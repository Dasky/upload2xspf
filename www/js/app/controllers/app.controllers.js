/**
 * Controllers
 * @author Dorian Yahouédéou <dorian@poc2prod.com>
 */

// App Controller
app.controller('AppController', function($rootScope) {
})

// Browser Controller
var browserCtrl = app.controller('BrowserController', ['$rootScope', '$scope', 'fileNavigator', 'fileSelector', 'oai', 'userService', '$location', 'playlistService', 
				  function($rootScope, $scope, fileNavigator, fileSelector, oai, userService, $location, playlistService) {
	$scope.step = 1;
	$scope.provider = userService.credentials;
	$scope.oaiIdentifier = null;
	$scope.playlistUrl = null;
	$scope.currentContext = {
		directory: '',
		file: '',
		path: '',
		directoryHash:'',
		fileHash:''
	};
	$scope.selectedPart = [];
	$scope.selectedFiles = [];
	$scope.getCurrentContext = function() {
		this.currentContext = fileNavigator.getContext();
	};
	$scope.addFile = function() {
		// clear the selection as we manage each file one by one
		$scope.removeFile(0);
		if (this.currentContext.file != '') {
			fileSelector.addFile(function(fileObj) {
				$scope.selectedFiles.push(fileObj);
				$scope.step = 2;
			});
		}
	};
	$scope.removeFile = function(pos) {
		$scope.selectedFiles.splice(pos, 1);
	};
	$scope.processFiles = function() {
		// move files to streaming directory
		fileSelector.processFiles($scope.selectedFiles, function(processedFiles) {
			// reference files to database
			fileSelector.publishFiles($scope.provider, $scope.recordOaiUrl, $scope.currentContext, $scope.selectedPart);
			// post process (workflow display control)
			$scope.postProcess(processedFiles);
		});
	};
	$scope.postProcess = function(list) {
		// update the files list
		var selectedFiles = $scope.selectedFiles;
		for (var i in list) {
			if (list[i].data.added[0].name == selectedFiles[i].fileName) {
				selectedFiles[i].processed = 1;
			}
		}
		$scope.selectedFiles = selectedFiles;
		// update the related items list
		$scope.getRecord();
		// change workflow step
		if ($scope.selectedFiles.length > 0)
			$scope.step = 5;
	};
	$scope.fileList = fileSelector.fileList;
	$scope.recordOaiUrl  = '';
	$scope.relatedItems =  [];
	$scope.playlistItems = [];
	$scope.getRecord = function() {
		oai.getRecord($scope.recordOaiUrl, function(data) {
			$scope.relatedItems = data.items;
			$scope.oaiIdentifier = data.oaiIdentifier;
			$scope.playlistUrl = data.playlistBaseUrl + data.oaiIdentifier;
			// update the playlist display
			$scope.loadPlaylist();
			$scope.step = 3;
		});
	};
	$scope.selectPart = function(index, obj) {
		$scope.selectedPart = {'index' : index, 'object' : obj};
		$scope.step = 4;
	};
	$scope.loadPlaylist = function() {
		playlistService.load($scope.provider.id, $scope.oaiIdentifier, function(playlistItems) {
			$scope.playlistItems = playlistItems;
		});
	};
	$scope.removeTrack = function(trackId) {
		playlistService.removeTrack(trackId, function() {
			$scope.loadPlaylist();
		});
	};
	$scope.disconnect = function() {
		userService.disconnect(userService.credentials.id, function(credentials) {
			userService.setCredentials(credentials);
			$location.path('/login');
		});
	};
}]);

// Login Controller
var LoginController = app.controller('LoginController', ['$scope', '$location', 'userService', 'fileNavigator', function($scope, $location, userService, fileNavigator) {
	$scope.login = null;
	$scope.password = null;
	$scope.authenticate = function() {
		userService.authenticate($scope.login, $scope.password, function(credentials) {
			userService.setCredentials(credentials);
			fileNavigator.setBaseAction(credentials.code);
			$location.path('/browser');	
		});
	};
}])

LoginController.authResolver = function(userService, $q) {
	var deferred = $q.defer();
	userService.isLoggedIn().then(function(userData) {
			userService.setCredentials(userData.data);
			deferred.resolve(userData.data);
		},
		function(reason) {
			deferred.reject();
		}
	);
	return deferred.promise;
};

// Dashboard Controller
var dashboardCtrl = app.controller('DashboardController', function($scope) {
	$scope.dashboardtitle = 'Welcome to Gamop';
	$scope.optionstitle = 'Your options';
})
