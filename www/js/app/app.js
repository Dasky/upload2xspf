/**
 * Application main.
 * @author Dorian Yahouédéou <dorian@poc2prod.com>
 */
var app = angular.module('app', ['ngRoute']);

// Setting app configuration
app.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
	$routeProvider
		.when('/', {
			redirectTo:'/login'
		})
		.when('/login', {
			controller:'LoginController',
			templateUrl:'login.tpl'
		})
		.when('/browser', {
			controller: 'BrowserController',
			templateUrl:'browser.tpl',
			resolve: {
				auth: LoginController.authResolver
			}
		});
	// HTTP Interceptors
	$httpProvider.interceptors.push('httpResponseInterceptor');
}]);

// Setting runtime rules
app.run(['$rootScope', '$location', function($rootScope, $location) {
	$rootScope.$on('$routeChangeSuccess', function() {
		var path = $location.path();
		if (path.indexOf('/') == 0) {
			var controller = path.substring(1);
			$rootScope.controller = controller;
		}
	});
}]);
