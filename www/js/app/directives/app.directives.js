/**
 * Directives and Services
 * TODO : separate the directives and the services in different files.
 * @author Dorian Yahouédéou <dorian@poc2prod.com>
 */
/* ******************************* DIRECTIVES ******************************* */
// Top level directives (error...)
app.directive('error', function($rootScope) {
	return {
		restrict:'E',
		link: function(scope) {
			scope.$on('$routeChangeError', function() {
				console.log("erreur de routage");
			})
		}
	};
});
// Secondary level directives
// Wrapper directive
app.directive('wrapper', function() {
	return {
		restrict:'E',
		replace:true,
		transclude:true,
		templateUrl:'wrapper.tpl'
	};
});
// Branding directive
app.directive('branding', function() {
	return {
		restrict:'E',
		replace:true,
		templateUrl:'branding.tpl'
	};
});
// Menu directive
app.directive('menu', function() {
	return {
		restrict:'E',
		replace:true,
		templateUrl:'menu.tpl'
	};
});

// filemanager directive
app.directive('fileBrowser', ['fileNavigator', function(fileNavigator) {
	return {
		restrict:'E',
		scope: false,
		replace: true,
		template:'<div></div>',
		link: function(scope, element, attrs) {
			var elf = $(element).elfinder({
				url : fileNavigator.baseAction,  // connector URL (REQUIRED)
				lang: attrs.lang,             // language (OPTIONAL)
				handlers : {
					select : function(event, elfinderInstance) {
						// use filenavigation service for setting context
						fileNavigator.setFile(elfinderInstance, event.data.selected);
						scope.$apply('getCurrentContext()');
					},
					open: function(event, elfinderInstance) {
						fileNavigator.setDirectory(elfinderInstance, event.data.cwd);
						fileNavigator.setDestinationDirectory(event.data.files);
						//scope.$apply('getCurrentContext()');
					},
				},
				height:230,
				uiOptions: {
					// toolbar
					toolbar : []
				},
				contextmenu: {
					navbar:[],
					cwd:['reload'],
					files:[]
				}	
					
			}).elfinder('instance');
		}
	}
}]);

// playlist directive
app.directive('playlist', ['playlistService', function(playlistService) {
	return {
		restrict: 'E',
		scope:false,
		templateUrl: 'playlist.tpl'
	}
}]);

// Select on click directive
app.directive('selectOnClick', function() {
	// linker function
	return function(scope, element, attrs) {
		element.bind('click', function() {
			this.select();
		});	
	}
});

/* *********************************** SERVICES *********************************** */
// Auth interceptor service // TODO
app.factory('httpResponseInterceptor', ['$q', '$location', '$window', function($q, $location, $window) {
	return {
		'request': function(config) {
				//var config.header = config.header || {};
				if ($window.sessionStorage.token) { 
					config.headers.Authorization = $window.sessionStorage.token;
					config.headers.id = $window.sessionStorage.id;
				}
				return (config);
			},
		'response': function(response) {
				return response || $q.when(response);
			},
		'responseError': function(rejection) {
				if (rejection.status === 401) {
					$location.path('/login');	
				}
				return $q.reject(rejection);
		}
	}
}]);
// User service : manage, user authentication
app.factory('userService', ['$rootScope', '$http', '$window', '$q', function($rootScope, $http, $window, $q) {
	return {
		authenticate: function(login, password, successCbk) {
			var postData = {
				'login':login,
				'password':password
			};
			$http({
				url:'/user/authenticate',
				method:'POST',
				data:postData,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			})
			.then(	function(response) {
					// set credentials and change location via browerController
					successCbk(response.data);	
				},
				function(response) {
					delete $window.sessionStorage.token;	
					console.log('unable to authenticate');
				}
			);
		},
		disconnect: function(id, successCbk) {
			$http.get('/user/disconnect/' + id)
			.then(	function(response) {
					delete $window.sessionStorage.token;
					delete $window.sessionStorage.id;
					console.log('user ' + id  + ' disconnected');
					successCbk({'id':'', 'code':'', 'connected':false, 'token':''});
				},
				function(response) {
				}
			);
		},
		credentials: {
			id:'',
			code:'',
			connected:false,
			token:''
		},
		setCredentials: function(userData) {
			this.credentials = userData;
			$rootScope.credentials = userData;
			$window.sessionStorage.token = userData.token;
			$window.sessionStorage.id = userData.id;		
		},
		isLoggedIn: function() {
			return $http.get('/user/getStatus')
				.success(function(response) {
					return response.data;
			     	})
				.error(function(response) {
					return $q.reject();
				});	
		}
	}
}]);
// File navigator service : manage the navigation on the file server
app.factory('fileNavigator', ['userService', function(userService) {
	return {
		currentContext: {
			directory: '',
			file: '',
			path: '',
			directoryHash: '',
			fileHash: ''
		},
		destinationHash: null,
		baseAction: 'connector/' + userService.credentials.code,
		setBaseAction: function(code) {
			this.baseAction = 'connector/' + code;
		},
		setFile: function(elfinderInstance, currentElt) {
			// is there a selected element?
			if (currentElt.length) {
				// get this selected element
				var currentElt = elfinderInstance.file(currentElt[0]);
				if (currentElt.mime != 'directory') {
					this.currentContext.file = currentElt.name;
					this.currentContext.fileHash = currentElt.hash;
					this.currentContext.path = this.currentContext.file;
				}
			}
		},
		setDirectory: function(elfinderInstance, currentElt) {
			if (currentElt.mime == 'directory') {
				this.currentContext.directory = currentElt.name;
				this.currentContext.directoryHash = currentElt.hash;
				this.currentContext.file = '';
				this.currentContext.path = this.currentContext.directory + ' ';
			}
		},
		setDestinationDirectory: function(files) {
			for (var i in files) {
				if (files[i].mime == 'directory' && files[i].volumeid == 'l2_') {
					this.destinationHash = files[i].hash;
				}
			}
		},
		getContext: function() {
			return this.currentContext;
		}
	}
}]);

// File selector service : select the file, add the file to the preprocess 
app.factory('fileSelector', ['fileNavigator', '$http', '$q', function(fileNavigator, $http, $q) {
	return {
		fileList: [],
		addFile: function(successCbk) {
                	var context = fileNavigator.getContext();
			successCbk({'path':context.path,
				    'fileName':context.file,
				    'fileHash':context.fileHash, 
				    'directoryHash':context.directoryHash,
				    'processed':0});
		},
		processFiles : function(fileArray, successCbk) {
			this.fileList = fileArray;
			var base_url = '/';
			var dstHash = fileNavigator.destinationHash;
			var deferred = $q.defer();
			var queryList = [];
			for (var i in fileArray) {
				queryList.push($http.get(base_url + fileNavigator.baseAction + '?cmd=paste&src=' + encodeURIComponent(fileArray[i].directoryHash) 
					+ '&dst=' 
					+ encodeURIComponent(dstHash) + '&targets[]=' 
					+ encodeURIComponent(fileArray[i].fileHash)));
			}
			$q.all(queryList).then(function (results) {
				var processedList = [];
				angular.forEach(results, function(result) {
					processedList.push(result);
				});
				successCbk(processedList);
			});
		},
		publishFiles : function(provider, oaiIdentifier, fileData, relatedItems) {
			var postData = {};
			postData.oaiUrl = oaiIdentifier;
			postData.providerId = provider.id;
			postData.providerCode = provider.code;
			postData.filename = fileData.file;
			postData.relatedItems = relatedItems;
			$http({
				url: '/media/publish',
				method: "POST",
				data: postData,
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			})
			.then(	function(response) {
					console.log('Upload2Xspf : file published.');
			      	},
				function(response) {
					console.log('Upload2Xspf: error.');
				}
			);
		}
	}
}]);

// Oai Service : manage the communication with the oai datawarehouses
app.factory('oai', ['$http', '$q', function($http, $q) {
	return {
		record: '',
		getRecord: function(oaiIdentifier, successCbk) {
			var deferred = $q.defer();
			deferred.promise.then(function() {
				$http.get('/oai/getRecord?url='  + encodeURIComponent(oaiIdentifier)).success(function(data) {
					successCbk(data);
				});
			});
			deferred.resolve();
		}
	}
}]);

// Playlist Service : manage a playlist data
app.factory('playlistService', ['$http', '$q', function($http, $q) {
	return {
		load: function(providerId, oaiIdentifier, successCbk) {
			$http.get('/playlist/get?providerId=' + encodeURIComponent(providerId) + '&oaiIdentifier=' + encodeURIComponent(oaiIdentifier)).success(function(data) {
				successCbk(data);
			});
		},
		removeTrack: function(trackId, successCbk) {
			$http.get('/playlist/removeTrack?trackId=' + encodeURIComponent(trackId)).success(function() {
				successCbk();
			}); 
		}	
	}
}]);
